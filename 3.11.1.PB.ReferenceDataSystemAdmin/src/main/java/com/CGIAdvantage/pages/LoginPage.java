package com.CGIAdvantage.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.CGIAdvantage.DriverFactory.DriverManager;
import com.CGIAdvantage.base.WindowFrames;
import com.CGIAdvantage.resources.Resources;;

public class LoginPage extends Resources{
	
	/**
	 * Login Page elements
	 */
	
	//Page Factory
	@FindBy(id="j_username")
	public WebElement userID;
	
	@FindBy(id="j_password")
	public WebElement password;
	
	@FindBy(className="ui-button-text")
	public WebElement singInButton;
	
	@FindBy(linkText="Forgot your password?")
	public WebElement forgotPasswordLink;
	
	/**
	 * Forget Password functionality elements 
	 */
	
	@FindBy(id="loginID")
	public WebElement forgetUserID;
	
	@FindBy(id="Next")
	public WebElement nextButton;
	
	@FindBy(id="Cancel")
	public WebElement cancelButton;
	
	@FindBy(id="emailAddress")
	public WebElement emailAddress;
	
	@FindBy(id="answer0")
	public WebElement firstAnswer;
	
	@FindBy(id="answer1")
	public WebElement secondAnswer;
	
	@FindBy(id="answer2")
	public WebElement thirdAnswer;
	
	@FindBy(id="Submit")
	public WebElement submitButton;
	
	
	// Now how to initialize these elements??
	// Interview Question how you initialize your variable >> using PageFactory class and initialize with 'driver'.
	public LoginPage(){
		PageFactory.initElements(DriverManager.getDriver(), this);
	}
	
	
	
	// login Page title
	
/*	public String loginPageTitle() {
		String title = driver.getTitle();
		
		return title;
	}*/
	
	
	// login method
	public void logIn(String username, String passwrd) throws InterruptedException {
		userID.sendKeys(username);
		password.sendKeys(passwrd);
		singInButton.click();
		
		Thread.sleep(2000);
//		WindowFrames.switchToHome();
		
	}
	
	//Verify Login
//	public boolean verifyLogin() {
//		try {
//			
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		
	}
