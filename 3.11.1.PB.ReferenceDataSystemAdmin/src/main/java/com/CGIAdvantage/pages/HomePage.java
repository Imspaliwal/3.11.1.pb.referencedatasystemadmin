package com.CGIAdvantage.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.CGIAdvantage.DriverFactory.DriverManager;
import com.CGIAdvantage.DriverFactory.Log;
import com.CGIAdvantage.resources.Resources;

public class HomePage extends Resources{
	
	@FindBy(xpath="//img[@alt='Advantage Performance Budgeting")
	public WebElement CGIAdvImage;
	
	@FindBy(linkText="Messages(0)")
	public WebElement messagesLink;
	
	@FindBy(linkText="Home")
	public WebElement homeLink;
	
	@FindBy(xpath="//img[@alt='Home page in a new window")
	public WebElement newHomeWindowLink;
	
	@FindBy(linkText="Help")
	public WebElement helpLink;
	
	@FindBy(linkText="Preferences")
	public WebElement preferencesLink;
	
	@FindBy(linkText="Edit Password")
	public WebElement editPasswordLink;
	
	@FindBy(linkText="Logout")
	public WebElement logoutLink;
	
	@FindBy(linkText="Go to top of page")
	public WebElement goToTopLink;
	
	//Reference Data Elements
	
	@FindBy(id="zp9wx3p")
	public WebElement referenceDataLink;
	
	@FindBy(id="zp9wx3p")
	public WebElement consolidationsLink;
	
/*	@FindBy(id="y5ev2rb")
	public WebElement diesnionsLink;*/
	
	@FindBy(linkText="Dimensions")
	public WebElement diesnionsLink;
	
	@FindBy(id="zcuvnu5")
	public WebElement budgetFormsLink;
	
	@FindBy(id="z1r1nxl")
	public WebElement performanceBudgetingLink;
	
	@FindBy(id="zjm8ph5")
	public WebElement budgetRankingLink;
	
	@FindBy(id="y98ekfb")
	public WebElement queriesLink;
	
	@FindBy(id="yizkjs9")
	public WebElement workflowLink;
	
	//SBFS Data Elements
	
	//Projection/Allocation Data Elements
	
	//Security Data Elements
	
	//Utilities Data Elements
	
	//Budget Task Data Elements
	
	//Dashboards Data Elements
	
	//Integration Data Elements
	
	//
	public HomePage() {
		PageFactory.initElements(DriverManager.getDriver(), this);
	}
	
//	This is verify Home Page
	public void verifyHomePage() {
		logoutLink.isDisplayed();
		Log.info("Home page dispalyed and object is:-" + logoutLink.toString());
	}
	
//	Verify Home Page Title
	public String verifyHomePageTitle() {
		
		return driver.getTitle();
	}
	

}
