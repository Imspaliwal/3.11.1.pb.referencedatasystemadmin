package com.CGIAdvantage.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.CGIAdvantage.DriverFactory.DriverManager;
import com.CGIAdvantage.resources.Resources;

public class ReferenceData extends Resources{
	
	ReferenceData refData;
	
	@FindBy(id="zp9wx3p")
	public static WebElement RefDataLink;
	
	@FindBy(id="y5ev2rb")
	public static WebElement dimensionsLink;
	
	
	
	
	

	
	public ReferenceData() {
		PageFactory.initElements(DriverManager.getDriver(), this);
	}
	
	//verify reference Data page title
	public String verifyReferenceDataTitle() {
		
		return driver.getTitle();
	}
	
	//

}
