package com.CGIAdvantage.pages.dimensions;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.CGIAdvantage.DriverFactory.DriverManager;
import com.CGIAdvantage.base.AppUtils;
import com.CGIAdvantage.factory.ConfigProvider;
import com.CGIAdvantage.pages.Dimension;
import com.CGIAdvantage.pages.HomePage;

public class FundMaintenance extends Dimension{
	
	HomePage homePage;
	Dimension dimensions;
	
	// Different Maint page button Web Elements
	@FindBy(id="yem7sbx")
	public static WebElement saveBtn;
	
	@FindBy(id="yelxusl")
	public static WebElement copyBtn;

	//Create Fund Maint
	@FindBy(id="z1v0dhi")
	public static WebElement codeTxt;

	@FindBy(id="z1utmso")
	public static WebElement nameTxt;

	@FindBy(id="zs6a9hv")
	public static WebElement SecOrgTxt;

	@FindBy(id="zlg403q")
	public static WebElement shortNameTxt;

	@FindBy(id="yh7wakv")
	public static WebElement DescTxtArea;

	@FindBy(id="yryg0sn")
	public static WebElement longDescTxtArea;

	@FindBy(id="ze587x3")
	public static WebElement ElementTypDrpDwn;

	@FindBy(id="zhff6u9")
	public static WebElement activeChkBox;

	@FindBy(id="ybkgs43")
	public static WebElement postableChkBox;

	@FindBy(id="ydlr8fo")
	public static WebElement usageDrpDwn;

	@FindBy(id="z7uhwon")
	public static WebElement multiYearChkbox;


	//Initialize page factory
	public FundMaintenance() {
		PageFactory.initElements(DriverManager.getDriver(), this);
	}

	//method to create Fund Dimensions Maintenance
	public void createFundDimension() throws InterruptedException {

		//ConfigProvider.appSpecific().createDimensions("Organization");
		ConfigProvider.appSpecific().jumpTohome();
		ConfigProvider.appSpecific().jumpToDimensions();
		fundMainLink.click();
		newBtn.click();
		codeTxt.sendKeys("Auto1");
		nameTxt.sendKeys("Auto1");
		SecOrgTxt.sendKeys("SEC_ORG");
		shortNameTxt.sendKeys("Auto1");
		DescTxtArea.sendKeys("Auto1");
		//longDescTxtArea.sendKeys("Auto1");

		Select select = new Select(ElementTypDrpDwn);
		select.selectByVisibleText("Fund");

		if(!activeChkBox.isSelected()) {
			activeChkBox.click();
		}

		if(!postableChkBox.isSelected()) {
			postableChkBox.click();
		}

		Select select1 = new Select(usageDrpDwn);
		select1.selectByVisibleText("Either");

		saveBtn.click();

		Thread.sleep(2000);
	}

}
