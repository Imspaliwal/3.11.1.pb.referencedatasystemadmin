package com.CGIAdvantage.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.CGIAdvantage.DriverFactory.DriverManager;
import com.CGIAdvantage.base.BaseClass;

public class TestUtil extends BaseClass{

	public static long PAGE_LOAD_TIMEOUT = 60;
	public static long IMPLICIT_WAIT = 30;

	//verification Elements
	@FindBy(xpath="//table[@class='message']/tbody/tr/td[2]")
	public static WebElement verMsgIcon;

	@FindBy(xpath="//table[@class='message']/tbody/tr/td[3]")
	public static WebElement verMsgSummary;

	@FindBy(xpath="//table[@class='message']/tbody/tr/td[4]")
	public static WebElement verMsgDetails;

	//Initialize page factory
	public TestUtil() {
		PageFactory.initElements(DriverManager.getDriver(), this);
	}

	//################### Window handles Utilities ##############################//

	//	This method will set focus to active window. 
	public static void setActiveWindow() {

		int count = 0;
		for (String handle : DriverManager.getDriver().getWindowHandles()) { // Last
			if (count == DriverManager.getDriver().getWindowHandles().size() - 1) {
				DriverManager.getDriver().switchTo().window(handle);
				break;
			}
			count++;
		}
	}

	//################### Drop Down Utilities ##############################//

	//	 This is the Select element utility to select element by Name.
	public static void selectElementByName(WebElement element, String Name) {
		Select selectitem = new Select(element);
		selectitem.selectByVisibleText(Name);
	}

	//################### Verifications Utilities ##############################//

	//	This is the verification utility to verify error and information message. 
	public static Boolean verifyRefData() {
		boolean msgDisplayedFlag = verMsgDetails.isDisplayed();

		if(msgDisplayedFlag) {
			if(verMsgDetails.getText().equalsIgnoreCase("Action was successful.")) {
				return true;
			}
			else {
				return false;
			}

		}
		else {
			return false;
		}
	}

	//	This is the verification utility to verify error and information message. 
	public static Boolean verifyRefDataError(String errMessage) {
		boolean msgDisplayedFlag = verMsgDetails.isDisplayed();
		String err = errMessage;

		if(msgDisplayedFlag) {
			if(verMsgDetails.getText().equalsIgnoreCase(err)) {
				return true;
			}
			else {
				return false;
			}

		}
		else {
			return false;
		}
	}

	//################### Capture Screen short Utilities ##############################//

	//
	public static String captureScreenShot(WebDriver driver, String screenShotName, String className) {

		TestUtil.setActiveWindow();
		DriverManager.setWebDriver(DriverManager.getDriver());
		TakesScreenshot ts = (TakesScreenshot) DriverManager.getDriver();
		File src = ts.getScreenshotAs(OutputType.FILE);

		File f = new File("../Screenshots/"+className+"/");

		String destination = "";

		if(!f.exists()) {
			f.mkdirs();
		}

		destination = "../Screenshots/"+ className +"/"+ screenShotName +".PNG";

		try {
//			FileHandler.copy(src, new File("../Screenshots/"+ className +"/"+ screenShotName +".PNG"));
			FileUtils.copyFile(src, new File("./Screenshots/"+ className +"/"+ screenShotName +".PNG"));

		} catch (IOException e) {
			System.out.println("Failed to take Screenshot" + e.getMessage());
		}

		System.out.println("Path of Screenshot" +f);

		return destination;

	}
}