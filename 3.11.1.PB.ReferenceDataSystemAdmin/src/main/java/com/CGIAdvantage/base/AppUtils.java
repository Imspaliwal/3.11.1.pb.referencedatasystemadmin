package com.CGIAdvantage.base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.CGIAdvantage.DriverFactory.DriverManager;
import com.CGIAdvantage.pages.HomePage;
import com.CGIAdvantage.util.TestUtil;

public class AppUtils extends TestUtil{
	
	
	@FindBy(linkText="Home")
	public WebElement homeLink;
	
	// Grid buttons WebElemensts
	@FindBy(id="yuja5ps")
	public static WebElement newBtn;
	
	@FindBy(id="yn74fne")
	public static WebElement editBtn;
	
	@FindBy(id="zb0fm05")
	public static WebElement deleteBtn;
	
	@FindBy(id="yn7dbjh")
	public static WebElement saveBtn;
	
	@FindBy(id="yn7fe1h")
	public static WebElement viewBtn;
	
	@FindBy(id="default_ItemCollection_SortCollectionMenu")
	public static WebElement sortBtn;
	
	//Reference Data Elements
	@FindBy(id="zp9wx3p")
	public WebElement referenceDataLink;
	
	@FindBy(id="zp9wx3p")
	public WebElement consolidationsLink;
	
	@FindBy(id="y5ev2rb")
	public WebElement dimensionsLink;
	
	@FindBy(id="zcuvnu5")
	public WebElement budgetFormsLink;
	
	@FindBy(id="z1r1nxl")
	public WebElement performanceBudgetingLink;
	
	@FindBy(id="zjm8ph5")
	public WebElement budgetRankingLink;
	
	@FindBy(id="y98ekfb")
	public WebElement queriesLink;
	
	@FindBy(id="yizkjs9")
	public WebElement workflowLink;
	
	//Dimensions Web Elements
	@FindBy (id="z7gcs8s")
	public static WebElement fundMainLink;
	
	@FindBy (id="z2d3vsa")
	public static WebElement orgMainLink;
	
	public AppUtils() {
		PageFactory.initElements(DriverManager.getDriver(), this);
	}
	
	
	public void jumpTohome() {
		//WindowFrames.switchToHome();
		homeLink.click();
	}
	
	public void jumpToDimensions() {
		dimensionsLink.click();
	}
	
	public void createNew() {
		
	}
	
	//############### Create Dimensions Utilities #################//
	
/*	public void createDimensions(String dimensionName) {
		jumpTohome();
		dimensionsLink.click();
		
		if(dimensionName == null) {
			
		}
		switch (dimensionName.toLowerCase()) {
		case "fund":
			
			fundMainLink.click();
			newBtn.click();
			break;
			
		case "organization":
			
			orgMainLink.click();
			newBtn.click();
			break;
			
		case "activity":
			
			fundMainLink.click();
			newBtn.click();
			break;
			
		case "budget object":
			
			fundMainLink.click();
			newBtn.click();
			break;
			
		case "time period":
			
			fundMainLink.click();
			newBtn.click();
			break;
			
		case "relationship edit":
			
			fundMainLink.click();
			newBtn.click();
			break;
			
		case "accounting template":
			
			fundMainLink.click();
			newBtn.click();
			break;
		}
	}*/

}
