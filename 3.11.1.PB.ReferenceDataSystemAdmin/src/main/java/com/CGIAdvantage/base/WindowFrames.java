package com.CGIAdvantage.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.CGIAdvantage.DriverFactory.DriverManager;
import com.CGIAdvantage.util.TestUtil;

public class WindowFrames extends TestUtil{
	
	private static WebDriver driver;
	
	public WindowFrames() {
		PageFactory.initElements(DriverManager.getDriver(), this);
	}
	
	public static void switchToHome() {
		DriverManager.getDriver().switchTo().frame("commonWidgetIFrame");
	}

}
