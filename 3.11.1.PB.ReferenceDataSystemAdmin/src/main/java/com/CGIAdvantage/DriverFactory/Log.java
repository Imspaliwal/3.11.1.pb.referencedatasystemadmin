package com.CGIAdvantage.DriverFactory;

import org.apache.log4j.Logger;

public class Log {
	
	//Initializing log4j instance
	private static Logger log = Logger.getLogger(Log.class.getClass());
	
	//We can use it stating test case
	public static void startTestCase(String testCaseName) {
		log.info("----- "+testCaseName+" -----"+" Starting");
	}
	
	//We can use it ending test case
	public static void endTestCase(String testCaseName) {
		log.info("----- "+testCaseName+" -----"+" Ended");
	}
	
	//info level log
	public static void info(String message) {
		log.info(message);
	}
	
	//warn level log
	public static void warn(String message) {
		log.warn(message);
	}
	
	//error level log
	public static void error(String message) {
		log.error(message);
	}
	
	//fatal level log
	public static void fatal(String message) {
		log.fatal(message);
	}
	
	//debug level log
	public static void dubeg(String message) {
		log.debug(message);
	}
	
}
