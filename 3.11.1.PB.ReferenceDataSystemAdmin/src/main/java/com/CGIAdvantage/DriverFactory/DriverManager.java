package com.CGIAdvantage.DriverFactory;

import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverManager {

	private static ThreadLocal<WebDriver> remoteWebDriver = new ThreadLocal<WebDriver>();
	static Logger log;
	static {
		log = Logger.getLogger(DriverManager.class);
	}
	public static WebDriver getDriver() {
		return remoteWebDriver.get();
	}

	public static void setWebDriver(WebDriver driver) {
		remoteWebDriver.set(driver);
	}


	/**
	 * Returns a string containing current browser name, its version and OS name.
	 * This method is used in the the *WebDriverListeners to change the test name.
	 * */
	public static String getBrowserInfo(){
		log.debug("Getting browser info");
		Capabilities cap = ((RemoteWebDriver) DriverManager.getDriver()).getCapabilities();
		String b = cap.getBrowserName();
		String os = cap.getPlatform().toString();
		String v = cap.getVersion();
		return String.format("%s v:%s %s", b, v, os);
	}
}

