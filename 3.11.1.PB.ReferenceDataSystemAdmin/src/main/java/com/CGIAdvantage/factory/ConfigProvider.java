package com.CGIAdvantage.factory;

import com.CGIAdvantage.base.AppUtils;
import com.CGIAdvantage.base.WindowFrames;
import com.CGIAdvantage.pages.Dimension;
import com.CGIAdvantage.pages.HomePage;
import com.CGIAdvantage.pages.LoginPage;
import com.CGIAdvantage.pages.dimensions.FundMaintenance;
import com.CGIAdvantage.resources.PropertyReader;

public class ConfigProvider {
	
	public static PropertyReader getPropData() {
		PropertyReader propData = new PropertyReader();
		return propData;
	}
	
	public static AppUtils appSpecific() {
		AppUtils appspec = new AppUtils();
		return appspec;
	}
	
	public static WindowFrames getWinFrame() {
		WindowFrames pgFrame = new WindowFrames();
		return pgFrame;
	}
	
	public static LoginPage loginPageData() {
		LoginPage loginPage = new LoginPage();
		return loginPage;
	}
	
	public static HomePage homePageData() {
		HomePage homePage = new HomePage();
		return homePage;
	}
	
	public static Dimension dimensionsData() {
		Dimension dimensions = new Dimension();
		return dimensions;
	}
	
	public static FundMaintenance fundMaintData() {
		FundMaintenance fundMaint = new FundMaintenance();
		return fundMaint;
	}
	
	

}
