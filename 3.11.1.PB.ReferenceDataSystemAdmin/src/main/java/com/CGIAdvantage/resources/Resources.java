package com.CGIAdvantage.resources;
	
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.CGIAdvantage.base.AppUtils;
import com.CGIAdvantage.dataProvider.ExcelReader;
import com.CGIAdvantage.util.TestUtil;

public class Resources extends AppUtils{
	
	public static File f;
	public static FileInputStream FI;
	
	public static Properties Repository = new Properties();
	
	public static ExcelReader LoginData;
	public static ExcelReader ReferenceData;
	
	public static void Initialize() throws IOException {
		LoginData = new ExcelReader(System.getProperty("user.dir")+"//TestData//Login.xlsx");
		
	}
	
	// Load the Properties file
	public void loadPropertiesFile() throws IOException {
		f = new File("./Configuration/Config.properties");
		FI = new FileInputStream(f);
		Repository.load(FI);
	}
	
	public static String getUrl() {
		String appUrl = Repository.getProperty("url");
		return appUrl;
	}
}
