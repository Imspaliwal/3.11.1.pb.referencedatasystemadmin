package com.CGIAdvantage.testcases;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CGIAdvantage.DriverFactory.DriverFactory;
import com.CGIAdvantage.DriverFactory.DriverManager;
import com.CGIAdvantage.DriverFactory.Log;
import com.CGIAdvantage.factory.ConfigProvider;
import com.CGIAdvantage.resources.PropertyReader;
import com.CGIAdvantage.resources.Resources;
import com.CGIAdvantage.util.TestUtil;

public class LoginPageTest extends Resources{

	WebDriver driver; 
	String className;

	public PropertyReader propertyReader = new PropertyReader();
	String SbrowserType = propertyReader.readTestData("browser");

	@Test(description = "LoginPB", priority = 1, enabled = true, groups = { "ReferenceData" })

	public void userLoginTest() throws InterruptedException
	{
		for(int LoginDataRecords = 2; LoginDataRecords <= LoginData.getRowCount("LoginCredential"); LoginDataRecords++) {

			try {
				//Login to PB application.
				Log.info("Read login Credentials");
				String uName = LoginData.getCellData("LoginCredential", "UserName", 2);
				String pwd = LoginData.getCellData("LoginCredential", "Password", 2);
				Log.info("User Name "+ uName + "& " + "Password "+ pwd);
				ConfigProvider.loginPageData().logIn(uName, pwd);
				//Verify login Successful.
				ConfigProvider.homePageData().verifyHomePage();
				//Verify Home Page Title.
				Assert.assertEquals(driver.getTitle(), "Advantage Performance Budgeting", "Home Page title not matched, Login Failed");
				Log.info("Login successfully!");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Test(description = "CreateDimensionsFund", priority = 2, enabled = true, groups = { "ReferenceData" })

	public void createDimensionsFund() throws InterruptedException
	{

		ConfigProvider.fundMaintData().createFundDimension();

		Assert.assertTrue(TestUtil.verifyRefData(), "Verify Fund Dimensions created.");
		//		Assert.assertTrue(TestUtil.verifyRefDataError("72 Duplicate Key Error."), "Verify Fund Dimensions has Duplicate Error.");

		Log.info("Fund Dimesions Created Successfully!.");

	}

	@BeforeTest

	public void initBrowser() throws IOException {
		Initialize();

	}

	@BeforeClass

	public void setup() throws IOException {

		className = this.getClass().getSimpleName();

		//Lunch specific browser type
		driver = DriverFactory.createInstance(SbrowserType);
		DriverManager.setWebDriver(driver);
		loadPropertiesFile();
		DriverManager.getDriver().get(ConfigProvider.getPropData().readTestData("url"));
		//PageFactory.initElements(new AjaxElementLocatorFactory(DriverManager.getDriver(), 60), this);

	}

	@AfterMethod

	public void tearDown(ITestResult result){

		//		Capture screen shots in end of the test cases

		TestUtil.captureScreenShot(DriverManager.getDriver(), result.getName(), className);

		//		Capture screen shots for Failed steps. 
	}

	@AfterClass

	public void publishHTMLReports() {

		//		DriverManager.getDriver().close();

		//		Publish HTML Reports
	}

}
